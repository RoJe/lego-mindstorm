﻿using System;
using System.Collections.Generic;

namespace legomind
{
	public class moveLog
	{
		public string moveType{ get; set; }
		public sbyte speedForward{ get; set; }
		public sbyte speedBackward{ get; set; }
		public int time{ get; set; }
	}
}

