﻿using System;

namespace legomind
{
	public class taskFive
	{
		//Move command class
		private moveCommands commands;

		//Base speeds
		private const sbyte baseSpeed = 30;
		private const sbyte baseBackSpeed = -30;

		public taskFive (ref moveCommands moveCommandClass)
		{

			// Initialize the move command class.
			this.commands = moveCommandClass;


			// Start the task
			this.startTask ();
		}

		private void startTask()
		{
			this.commands.align (-30);
			this.commands.goForward (baseSpeed, 5000);
			this.commands.discoSensor ("Black", 20);
			this.commands.goRight (5, -5, 500);
			this.commands.goForward (30, 1500);
			this.commands.goback (-30, 1500);
			this.commands.goRight (-5, 5, 500);
			this.commands.align (-30);
			this.commands.stop (-1);

		}
	}
}

