﻿using System;
using MonoBrickFirmware;
using MonoBrickFirmware.Display.Dialogs;
using MonoBrickFirmware.Display;
using MonoBrickFirmware.Movement;
using MonoBrickFirmware.Sensors;
using System.Threading;

using System.Collections.Generic;

namespace legomind
{
	public class taskTwo
	{
		//Move command class
		private moveCommands commands;

		//Base speeds
		private const sbyte baseSpeed = 30;
		private const sbyte baseBackSpeed = -30;

		public taskTwo (ref moveCommands moveCommandClass)
		{
			// Initialize the move command class.
			this.commands = moveCommandClass;

			// Start the task
			this.startTaskTwo ();
		}

		private void startTaskTwo()
		{ 
			// Start from the point where taskOne stopped
			this.commands.forksControl(20, 40);
			this.commands.discoSensor ("Black", 30);

			this.commands.goback (-20, 300);
			this.commands.stop (-1);

			this.commands.goRightBack (5,-5,3175);

			this.commands.align (-30);

			this.commands.goForward (20, 1000);

			this.commands.stop (-1);

			this.commands.forksControl (-20,750); 
			this.commands.stop (-1);

			this.commands.goForward (20,1700);

			this.commands.stop (-1);

			this.commands.forksControl (20, 450); 

			this.commands.stop (-1); 

			this.commands.goback (-20, 1000);

			this.commands.goLeftBack (5, -5, 3175);

			this.commands.align (-30);

			this.commands.stop (-1);

		}

	}
}

