﻿using System;
using MonoBrickFirmware;
using MonoBrickFirmware.Display.Dialogs;
using MonoBrickFirmware.Display;
using MonoBrickFirmware.Movement;
using MonoBrickFirmware.Sensors;
using System.Threading;

using System.Collections.Generic;

namespace legomind
{
	public class taskOne
	{
		//Move command class
		private moveCommands commands;

		//Base speeds
		private const sbyte baseSpeed = 30;
		private const sbyte baseBackSpeed = -30;


		public taskOne (ref moveCommands moveCommandClass)
		{
			// Initialize the move command class.
			this.commands = moveCommandClass;
		

			// Start the task
			this.startTask ();
		}

		/**
		 * Start the the first task. Make it move and stuff.
		 */
		private void startTask()
		{

			this.commands.goback (baseBackSpeed, -1);

			this.commands.align (baseBackSpeed);

			this.commands.goForward (baseSpeed, 3550); // afstand tot het hok preciezer bepalen voor 'missie 5 filter'
			this.commands.goRightBack (-5, 5, 3150);

			this.commands.touchLeft (baseBackSpeed);


			this.commands.goForward (baseSpeed, 1250);
			this.commands.goRight (-5, 5, 3100);
			this.commands.goForward (baseSpeed, 4000);
			this.commands.stop (-1);
		/*
			Lcd.Clear ();
			Lcd.Update ();
			*/
		}
	}
}

