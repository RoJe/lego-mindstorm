﻿using System;
using MonoBrickFirmware;
using MonoBrickFirmware.Display.Dialogs;
using MonoBrickFirmware.Display;
using MonoBrickFirmware.Movement;
using MonoBrickFirmware.Sensors;
using System.Threading;

using System.Collections.Generic;

namespace legomind
{
	public class TaskFour
	{
		//Move command class
		private moveCommands commands;

		//Touchsensors
		private EV3TouchSensor touchSensorOne;
		private EV3TouchSensor touchSensorTwo;
		private EV3ColorSensor colorSensor;

		//Base speeds
		private sbyte baseSpeed;
		private sbyte baseBackSpeed;
		
		public TaskFour (ref moveCommands moveCommandClass)
		{
			// Initialize the move command class.
			this.commands = moveCommandClass;

			// Set the base speeds for the robot for future refrance.
			this.baseSpeed = 30;
			this.baseBackSpeed = -30;



			// Start the task
			this.StartTasktFour ();
		}


		private void StartTasktFour ()
		{
			// Start from basepoint 
			this.commands.forksControl(20, 1500);
			this.commands.stop (-1);

			this.commands.goForward(10,250);
			this.commands.stop (-1);
			this.commands.align (-30);
			this.commands.goForward (20, 1500);
			this.commands.stop (-1);
			this.commands.goLeftBack (5, -5, 3350);
			this.commands.stop (-1);
			this.commands.align (-30); 
			
			// Begin de missie
			this.commands.goForward (50, 3000);
			this.commands.goForward (40, 250);
			this.commands.goForward (30, 250);
			this.commands.goForward (20, 250);
			this.commands.stop (-1);
			this.commands.discoSensor ("Black", 30);

			// 45 graden bocht maken
			this.commands.goForward (30, 400);
			this.commands.stop (-1);
			this.commands.goRight (5, -5, 1000);
			this.commands.goForward (30, 1000);
			this.commands.stop (-1);
			this.commands.goLeft (5, -5, 1000);

			// verder met de opdracht
			this.commands.discoSensor ("Black", 30);
			this.commands.goLeft (10, -10, 500);
			this.commands.goForward (baseSpeed, 1000);
			this.commands.stop (-1);
			this.commands.goLeft (5, -5, 1600);
			this.commands.goForward (30, 900);
			this.commands.stop (-1);
			this.commands.goLeft (5, -5, 1200);

			this.commands.forksControl(-20, 800);
			this.commands.stop (-1);


			// begin de weg terug

			this.commands.goback (-20, 800);
			this.commands.stop (-1);
			this.commands.forksControl(20, 1300);
			this.commands.stop (-1);
			this.commands.goForward (10, 200);

			this.commands.goRight (5, -5, 1600);

			this.commands.goback(-20, 900);

			this.commands.goRight (5, -5, 800);
			this.commands.stop (-1);

			this.commands.goback (-30, 1000);
			this.commands.stop (-1);

			this.commands.goRight (5, -5, 1400);
			this.commands.stop (-1);

			this.commands.goback (-30, 500);
			this.commands.stop (-1);

			this.commands.goRight (5, -5, 1600);
			this.commands.stop (-1);

			this.commands.discoSensor ("Black", -30);

			this.commands.goForward (baseBackSpeed, 1000);

			this.commands.goRightBack (10, -10, 1750);

			this.commands.align (baseBackSpeed);
			this.commands.goForward(10, 2500);
			this.commands.goRight(10, -10, 1750);
			this.commands.align(baseBackSpeed);

			this.commands.forksControl (-20, 1000);

			this.commands.stop (-1);



		}
	}
}

