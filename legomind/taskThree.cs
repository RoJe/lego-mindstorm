﻿using System;
using MonoBrickFirmware;
using MonoBrickFirmware.Display.Dialogs;
using MonoBrickFirmware.Display;
using MonoBrickFirmware.Movement;
using MonoBrickFirmware.Sensors;
using System.Threading;

using System.Collections.Generic;

namespace legomind
{
	public class taskThree
	{
		//Move command class
		private moveCommands commands;

		//Base speeds
		private const sbyte baseSpeed = 30;
		private const sbyte baseBackSpeed = -30;

		public taskThree (ref moveCommands moveCommandClass)
		{
			// Initialize the move command class.
			this.commands = moveCommandClass;

			// Start the task
			this.startTaskThree();
		}
		private void startTaskThree()
		{ 
			// Start from basepoint 
			this.commands.goForward(10,500);
			this.commands.stop (-1);

			this.commands.align (-30);

			this.commands.goForward (20, 1200);
			this.commands.stop (-1);

			this.commands.goLeftBack (-5, 5, 3180);
			this.commands.stop (-1);

			this.commands.align (-30);

			//Begin de missie
			this.commands.goForward (50,2100);
			this.commands.stop (-1);

			this.commands.goRight (5,-5,1500);
			this.commands.stop (-1);

			this.commands.goForward (20,3100);
			this.commands.stop (-1);

			this.commands.goLeft (-5,5,1000);
			this.commands.stop (-1);

			this.commands.forksControl (-20, 150);
			this.commands.stop (-1);

			this.commands.goForward (20,1100);
			this.commands.stop (-1);

			this.commands.forksControl (20, 250);
			this.commands.stop (-1);

			this.commands.goLeft (5,-5,1000);
			this.commands.stop (-1);

			this.commands.forksControl (-20, 650);
			this.commands.stop (-1);

			// Begin met het terug rijden van de robot
			this.commands.goback (-20, 1500);
			this.commands.stop (-1);

			this.commands.goRightBack (5, -5, 1000);
			this.commands.stop (-1);

			this.commands.goback (-30, 4000);
			this.commands.stop (-1);

			this.commands.goRightBack (5, -5, 900);
			this.commands.stop (-1);

			this.commands.goback (-20, 1000);
			this.commands.goLeft (10, -10, 450);
			this.commands.stop (-1);

			this.commands.align (-30);
			this.commands.stop (-1);


		}

	}
}

