﻿using System;
using MonoBrickFirmware;
using MonoBrickFirmware.Display.Dialogs;
using MonoBrickFirmware.Display;
using MonoBrickFirmware.Movement;
using MonoBrickFirmware.Sensors;
using System.Threading;

using System.Collections.Generic;


namespace legomind
{
	public class moveCommands
	{
		private Motor wheelRight;
		private Motor wheelLeft;
		private Motor lifting;

		private EV3TouchSensor touchSensorOne;
		private EV3TouchSensor touchSensorTwo;
		private EV3ColorSensor colorSensor;

		private List<moveLog> log;

		public moveCommands (ref Motor wheelLeft, ref Motor wheelRight, ref Motor lifting, ref EV3TouchSensor touchSensorOne, ref EV3TouchSensor touchSensorTwo, ref EV3ColorSensor colorSensor, ref List<moveLog> log)
		{
			//Initialize the motor class.
			this.wheelRight = wheelRight;
			this.wheelLeft = wheelLeft;
			this.lifting = lifting;
			this.colorSensor = colorSensor;

			//Initialize the sensor class.
			this.touchSensorOne = touchSensorOne;
			this.touchSensorTwo = touchSensorTwo;
			this.colorSensor = colorSensor;

			this.log = log;
			 
		}
		/**	
		 * 	Make the robot go left
		 * 
		 * @sbyte speed	How much does the crane need to move.
		 * @int time 	the time for how long it needs to do this command.
		 */
		public int forksControl(sbyte speedUp, int time)
		{
			this.lifting.SetSpeed (speedUp);
			if (time > 0)
			{
				Thread.Sleep (time);
			}
			return 0;
		}

		/**
		 *	Make the robot go left by spinning one wheel back and the other one forward.
		 *
		 *	@sbyte speedBack	the speed of which the wheel needs to spin backwards.
		 *	@speed speedForward	The speed of which teh wheel needs to spin forward.
		 *	@int time			The time for how long it needs to do this command.
		 */
		public int goLeft(sbyte speedBack, sbyte speedFoward, int time)
		{
			
			this.wheelLeft.SetSpeed (speedFoward);
			this.wheelRight.SetSpeed (speedBack);
			if (time > 0) {
				Thread.Sleep (time);
			}
			this.log.Add (new moveLog{ moveType = "goLeft", speedForward = speedFoward, speedBackward = speedBack, time = time });

			return 0;
		}

		/**
		 * Make the robot go back left by making both wheels spin in oppocit directions.
		 * 
		 *	@sbyte speedBack	the speed of which the wheel needs to spin backwards.
		 *	@sbyte speedForward	The speed of which teh wheel needs to spin forward.
		 *	@int time			The time for how long it needs to do this command.
		 */
		public int goLeftBack(sbyte speedBack, sbyte speedFoward, int time)
		{
			this.wheelLeft.SetSpeed (speedBack);
			this.wheelRight.SetSpeed (speedFoward);
			if (time > 0) {
				Thread.Sleep (time);
			}
			this.log.Add (new moveLog{ moveType = "goLeftBack", speedForward = speedFoward, speedBackward = speedBack, time = time });

			return 0;
		}

		/**
		 * Make the robot run right by making bothe wheels spin in oppocite directions.
		 * 
		 *	@sbyte speedBack	the speed of which the wheel needs to spin backwards.
		 *	@sbyte speedForward	The speed of which teh wheel needs to spin forward.
		 *	@int time			The time for how long it needs to do this command.
		 */
		public int goRight(sbyte speedBack, sbyte speedFoward, int time)
		{
			this.wheelLeft.SetSpeed (speedBack);
			this.wheelRight.SetSpeed(speedFoward);
			if (time > 0 ) {
				Thread.Sleep (time);
			}
			this.log.Add (new moveLog{ moveType = "goRight", speedForward = speedFoward, speedBackward = speedBack, time = time });

			return 0;
		}

		/**
		 * Make the robot go back right by making both wheels spin in oppocit directions.
		 * 
		 *	@sbyte speedBack	the speed of which the wheel needs to spin backwards.
		 *	@sbyte speedForward	The speed of which teh wheel needs to spin forward.
		 *	@int time			The time for how long it needs to do this command.
		 */
		public int goRightBack(sbyte speedBack, sbyte speedForward, int time)
		{
			this.wheelLeft.SetSpeed (speedForward);
			this.wheelRight.SetSpeed(speedBack);
			if (time > 0) {
				Thread.Sleep (time);
			}
			this.log.Add (new moveLog{ moveType = "goRightBack", speedForward = speedForward, speedBackward = speedBack, time = time });

			return 0;
		}

		/**
		 * Make the robot go forward! ( LEEERRRROOOOYYYY!!!! )
		 * 
		 * @sbyte speed		the speed it needs to go forward ( OVER 9000! )
		 * @int time 		the time for how long it needs to do this command
		 */
		public int goForward(sbyte speed, int time)
		{
			this.wheelLeft.SetSpeed (speed);
			this.wheelRight.SetSpeed (speed);
			if (time > 0) {
				Thread.Sleep (time);
			}
			this.log.Add (new moveLog{ moveType = "goForward", speedForward = speed, speedBackward = 0, time = time });

			return 0;
		}

		/**
		 * Make the robot go back. 
		 * 
		 * @sbyte speed		the speed it needs to go backwards.
		 * @int time 		the time for how long it needs to do this command.
		 */
		public int goback(sbyte speed, int time)
		{
			this.wheelLeft.SetSpeed (speed);
			this.wheelRight.SetSpeed (speed);
			if (time > 0) {
				Thread.Sleep (time);
			}
			this.log.Add (new moveLog{ moveType = "goback", speedForward = 0, speedBackward = speed, time = time });

			return 0;
		}

		/**
		 * Make the robot stop.
		 * 
		 * @int time 		the time for how long it needs to do this command.
		 */
		public int stop( int time )
		{
			this.wheelLeft.Brake ();
			this.wheelRight.Brake ();
			this.lifting.Brake ();

			if (time > 0) {
				Thread.Sleep (time);
			}
			this.log.Add (new moveLog{ moveType = "stop", speedForward = 0, speedBackward = 0, time = time });

			return 0;
		}

		/**
		 * Detect if the left sencor is being touched. If not? keep going backwards untill we do.
		 * 
		 * @sbyte speed		The speed it needs to move.
		 */
		public int touchLeft(sbyte speed)
		{
			int touchTwo = this.touchSensorTwo.Read ();
			while (touchTwo == 0) {
				touchTwo = this.touchSensorTwo.Read ();
				this.goback (speed, -1);
			}

			return 0;
		}

		/**
		 * Detect if the right sencor is being touched. If not? keep going backwards untill we do.
		 * 
		 * @sbyte speed		The speed it needs to move.
		 */
		public int touchRight(sbyte speed)
		{
			int touchOne = this.touchSensorOne.Read ();
			while (touchOne == 0) {
				touchOne = this.touchSensorTwo.Read ();
				this.goback (speed, -1);
			}

			return 0;
		}

		/**
		 * Detect if both both sencors are pressed. If not? Keep going backwards.
		 * 
		 * @sbyte speed		The speed it needs to move.
		 */
		public int align (sbyte baseBackSpeed)
		{
			int touchOne = this.touchSensorOne.Read ();
			int touchTwo = this.touchSensorTwo.Read ();

			while (touchOne == 0 || touchTwo == 0) {
				touchOne = this.touchSensorOne.Read ();
				touchTwo = this.touchSensorTwo.Read ();

				if (touchOne == 1 && touchTwo == 0) {
					this.goRightBack (-5, 5, -1);
				}

				if (touchOne == 0 && touchTwo == 0) {
					this.goback (baseBackSpeed, -1);
				}

				if (touchOne == 0 && touchTwo == 1) {
					this.goLeftBack (-5, 5, -1);
				}
			}

			return 0;
		}

		/**
		 * Detect the color were looking for underneath the robot. If not? Keep going forward until we do.
		 * 
		 * @string Color		The color were looking for.
		 * @sbyte moveSpeed		The speed we need to move.
		 */
		public int discoSensor (String Color, sbyte moveSpeed)
		{
			String colorSensor = this.colorSensor.ReadAsString();

			while (true) 
			{
				colorSensor = this.colorSensor.ReadAsString();

				if (colorSensor == Color) 
				{
					this.stop (-1);
					break;
				} 
				this.goForward (moveSpeed, -1);
			}

			return 0;
		}

	}
}

