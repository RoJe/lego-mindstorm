﻿
using System;
using MonoBrickFirmware;
using MonoBrickFirmware.Display.Dialogs;
using MonoBrickFirmware.Display;
using MonoBrickFirmware.Movement;
using MonoBrickFirmware.Sensors;
using System.Threading;

using System.Collections.Generic;

namespace legomind
{
	class MainClass
	{
		// Declare the necessary classes for teh robot to work.
		private static Motor lifting;
		private static Motor wheelLeft;
		private static Motor wheelRight;

		private static EV3TouchSensor touchSensorOne;
		private static EV3TouchSensor touchSensorTwo;

		private static EV3ColorSensor colorSensor;

		//A debug class to log the activities of the robot
		private static List<moveLog> log;
		//A class containing all the move commands for the robot. IOW goLeft, goRight etc..
		private static moveCommands commands;

		//All the missions stored in their own class and stuff
		private static taskOne taskOne;
		private static taskTwo taskTwo;
		private static taskThree taskThree;
		private static TaskFour TaskFour;
		private static taskFive taskFive;

		public static void Main (string[] args)
		{ 
			// Initialize all the classes related to engines on the robot
			lifting = new Motor (MotorPort.OutA);
			wheelLeft = new Motor(MotorPort.OutB);
			wheelRight = new Motor (MotorPort.OutD);

			//Initialize the classes related to the touch sensors.
			touchSensorOne = new EV3TouchSensor (SensorPort.In1);
			touchSensorTwo = new EV3TouchSensor (SensorPort.In2);

			//Initialize the colorsensor class
			colorSensor = new EV3ColorSensor (SensorPort.In3, ColorMode.Color); 

			// Initialize the log for the robot.
			log = new List<moveLog>();

			// Calibratie voor de forksControl
			/*
			lifting.SetSpeed (20);
			Thread.Sleep (450);
			lifting.Brake ();
			Thread.Sleep (2000);

			lifting.SetSpeed (-20);
			Thread.Sleep (550);
			lifting.Brake ();
*/


			// Initialize the robot move library.
			commands = new moveCommands(ref wheelLeft, ref wheelRight, ref lifting, ref touchSensorOne, ref touchSensorTwo, ref colorSensor, ref log);

			//Start task one : Filter
			//taskOne = new taskOne (ref commands);

			//Start task two : Removal broken pipe
			//taskTwo = new taskTwo (ref commands);

			//Start task Three : Laat het regenen
			//taskThree = new taskThree (ref commands);

			//Start Task Four : waterput plaatsen
			TaskFour = new TaskFour (ref commands);

			// Start Task Five
			//taskFive = new taskFive (ref commands);




			/**
			 * This is here for later use! 
			 * Why is this here? Cuz swaggy
			 * 
			 * retraceSteps rSteps = new retraceSteps (commands, log);
			 */
		}
	}
}

